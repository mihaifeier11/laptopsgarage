CREATE PROCEDURE CRUD_AplicatiiCompatibile
	@lid INT,
	@aid INT
AS 
BEGIN
	-- CREATE - INSERT
	INSERT INTO aplicatiiCompatibile(lid, aid) VALUES (@lid, @aid)

	-- READ - SELECT
	SELECT * FROM aplicatiiCompatibile

	-- DELETE
	UPDATE aplicatiiCompatibile SET lid = 1 WHERE lid = 2

	-- DELETE
	DELETE FROM aplicatiiCompatibile WHERE lid = @lid AND aid = @aid
END


CREATE PROCEDURE CRUD_Aplicatii
	@nume VARCHAR(20),
	@scop VARCHAR(20),
	@spatiuNecesarGB INT, 
	@noOfRows INT
AS
BEGIN
	IF dbo.ValidateSpatiuNecesar(@spatiuNecesarGB) = 0
		print 'Spatiul necesar nu poate fi negativ.'
	ELSE
	BEGIN
		-- CREATE - INSERT
		DECLARE @n INT = 1
		
		WHILE @n <= @noOfRows
		BEGIN
			INSERT INTO Aplicatii(nume, scop, spatiuNecesarGB) VALUES (@nume, @scop, @spatiuNecesarGB)
			SET @n = @n + 1
		END

		-- READ - SELECT
		SELECT * FROM Aplicatii

		-- UPDATE
		UPDATE Aplicatii SET spatiuNecesarGB = 10 WHERE spatiuNecesarGB < 10

		-- DELETE
		DELETE FROM Aplicatii WHERE spatiuNecesarGB = @spatiuNecesarGB

	END
END




EXEC CRUD_Procesoare 'Intel', 'i5', 5.5, 10

EXEC CRUD_AplicatiiCompatibile 5, 1

EXEC CRUD_Aplicatii 'Sublime', 'Informatica', 23, 10

EXEC CRUD_PlaciVideo 'nVidia', 'GTX 1060', 'dedicata', 'GDDR5', 8, 10

EXEC CRUD_Mouse 'SteelSeries Rival 100', 100, 10

DELETE FROM Laptopuri WHERE lid > 12


SELECT * FROM Laptopuri
ORDER BY lid

CREATE INDEX In










CRUD_Procesoare 'Intel', 'i5', 5.5, 10

SELECT * FROM Procesoare
