﻿-- Creați o bază de date pentru gestiunea mersului trenurilor. Baza de date va conține informații despre rutele tuturor trenurilor. 
-- Entitățile de interes pentru domeniul problemei sunt: trenuri, tipuri de tren, stații și rute.-- Fiecare tren are un nume și aparține unui tip. Tipul trenului are o descriere.
-- Fiecare rută are un nume, un tren asociat și o listă de stații cu ora sosirii și ora plecării pentru fiecare stație. 
-- Ora sosirii și ora plecării sunt reprezentate ca perechi oră/minut (exemplu: trenul ajunge la 5 PM și pleacă la 5:10 PM).-- Stația are un nume.

--a) Scrieți un script SQL care creează un model relațional pentru a reprezenta datele. 
CREATE TABLE TipTren(ttid INT IDENTITY PRIMARY KEY,
	descriere VARCHAR(100))

CREATE TABLE Trenuri(tid INT IDENTITY PRIMARY KEY,
	nume VARCHAR(50),
	ttid INT FOREIGN KEY REFERENCES TipTren(ttid))

CREATE TABLE Statii(sid INT IDENTITY PRIMARY KEY,
	nume VARCHAR(50))

CREATE TABLE Rute(rid INT IDENTITY PRIMARY KEY,
	nume VARCHAR(50),
	tid INT FOREIGN KEY REFERENCES Trenuri(tid))

CREATE TABLE Opriri(rid INT FOREIGN KEY REFERENCES Rute(rid),
	sid INT FOREIGN KEY REFERENCES Statii(sid),
	oraSosirii TIME,
	oraPlecarii TIME,
	CONSTRAINT pk_Opriri PRIMARY KEY(rid, sid))

INSERT INTO TipTren VALUES('description 1'), ('descrition 2')
INSERT INTO Trenuri values ('InterRegio', 1), ('Intercity', 1), ('Regio', 2)
INSERT INTO Statii values ('Cluj-Napoca'), ('Brasov'), ('Bucuresti')
Insert into Rute values ('Sighisoara', 1), ('Medias', 2)
INSERT Opriri VALUES(1,1,'12:00:00', '18:00:00'), (1,2,'15:30:00', '22:42:00'),
(2,2,'08:05:00', '21:48:00')INSERT INTO Opriri VALUES (1, 3, '12:00:00', '18:00:00')

-- b) Creați o procedură stocată care primește o rută, o stație, ora sosirii, ora plecării și adaugă noua stație rutei. 
-- Dacă stația există deja, se actualizează ora sosirii și ora plecării. 

CREATE PROCEDURE adauga_statie
@ruta INT,
@statie INT,
@oraSosirii TIME,
@oraPlecarii TIME
AS
BEGIN
	DECLARE @count INT = 0
	SELECT @count = COUNT(*) FROM Opriri WHERE rid = @ruta AND sid = @statie
	IF @count > 0
	BEGIN
		UPDATE Opriri SET oraSosirii = @oraSosirii, oraPlecarii = @oraPlecarii WHERE rid = @ruta AND sid = @statie
		PRINT 'Oprirea exista deja, a fost updatata'
	END
	ELSE
	BEGIN 
		INSERT INTO Opriri VALUES(@ruta, @statie, @oraSosirii, @oraPlecarii)
		PRINT 'Oprirea a fost adaugata'
	END


END

adauga_statie 1,1,'00:00:00', '00:00:00'


-- c) Creați un view care afișează numele rutelor care conțin toate stațiile.

CREATE VIEW ruteToateStatiile
AS
	SELECT R.nume FROM Rute R 
	FULL JOIN Opriri O ON R.rid = O.rid
	GROUP BY R.nume 
	HAVING COUNT(O.rid) = (SELECT COUNT(sid) FROM Statii)


SELECT * FROM ruteToateStatiile


--d) Creați o funcție care afișează toate stațiile care au mai mult de un tren la un anumit moment din zi. 

CREATE FUNCTION StatiiRute()
RETURNS TABLE
AS
RETURN
	SELECT S.nume FROM Statii S
	INNER JOIN Opriri O ON S.sid = O.sid
	WHERE (SELECT COUNT(*) FROM Opriri O2 WHERE O2.rid != O.rid AND O.sid = O2.sid AND ((O.oraSosirii >= O2.oraSosirii AND O.oraSosirii <= O2.oraPlecarii) OR (O2.oraSosirii >= O.oraSosirii AND O2.oraSosirii <= O.oraPlecarii))) > 0
	GROUP BY S.nume


SELECT * FROM StatiiRute()


-- extra) Create a function that lists the names of the stations with more than R routes, where R>=1 is a function parameter. 

CREATE FUNCTION MaiMulteRuteStatie(@r int)
RETURNS TABLE
AS
RETURN
	SELECT S.nume FROM Statii S
	INNER JOIN Opriri O ON O.sid = S.sid
	GROUP BY S.nume
	HAVING COUNT(O.rid) > @r
	
SELECT * FROM MaiMulteRuteStatie(1)
