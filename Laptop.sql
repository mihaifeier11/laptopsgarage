CREATE TABLE Procesoare(pid INT,
						nume VARCHAR(20),
						socket VARCHAR(10),
						frecventa FLOAT,
						PRIMARY KEY(pid))

CREATE TABLE PlaciVideo(pvid INT,
						nume VARCHAR(20),
						frecventa VARCHAR(20),
						tipMemorie VARCHAR(20),
						dimensiuneMemorie VARCHAR(20)
						PRIMARY KEY(pvid))

CREATE TABLE Memorii(mid INT,
					nume VARCHAR(20),
					tip VARCHAR(20),
					dimensiune VARCHAR(20),
					frecventa VARCHAR(20),
					PRIMARY KEY(mid))

CREATE TABLE SSD(ssdid INT,
				nume VARCHAR(20),
				interfata VARCHAR(20),
				capacitate VARCHAR(20),
				arhitectura VARCHAR(20),
				PRIMARY KEY(ssdid))

CREATE TABLE HDD(hid INT,
				nume VARCHAR(20),
				interfata VARCHAR(20),
				capacitate INT,
				buff INT,
				PRIMARY KEY(hid))

CREATE TABLE PlaciSunet(psid INT,
						nume VARCHAR(20),
						numarCanale INT,
						interfata VARCHAR(20),
						PRIMARY KEY(psid))

CREATE TABLE Aplicatii(aid INT,
					   nume VARCHAR(20),
					   scop VARCHAR(20),
					   spatiuNecesar INT,
					   PRIMARY KEY(aid))

CREATE TABLE SistemeOperare(soid INT,
							nume VARCHAR(20),
							tip VARCHAR(20),
							PRIMARY KEY(soid))

CREATE TABLE Laptopuri(lid INT,
					   nume VARCHAR(20),
					   pret float,
					   pid INT,
					   pvid INT,
					   mid INT,
					   ssdid INT,
					   hid INT,
					   psid INT,
					   soid INT,
					   PRIMARY KEY(lid),
					   FOREIGN KEY(pid) REFERENCES Procesoare(pid),
					   FOREIGN KEY(pvid) REFERENCES PlaciVideo(pvid),
					   FOREIGN KEY(mid) REFERENCES Memorii(mid),
					   FOREIGN KEY(ssdid) REFERENCES SSD(ssdid),
					   FOREIGN KEY(hid) REFERENCES HDD(hid),
					   FOREIGN KEY(psid) REFERENCES PlaciSunet(psid),
					   FOREIGN KEY(soid) REFERENCES SistemeOperare(soid))

CREATE TABLE Oferte(oid INT,
				    lid INT,
					pret FLOAT,
					rataLunara Float,
					PRIMARY KEY(oid),
					FOREIGN KEY(lid) REFERENCES Laptopuri(lid))

CREATE TABLE aplicatiiCompatibile(lid int REFERENCES Laptopuri(lid),
								  aid int REFERENCES Aplicatii(aid),
								  PRIMARY KEY(lid, aid))

ALTER TABLE Laptopuri ADD oid INT
ALTER TABLE Laptopuri ADD FOREIGN KEY(oid) REFERENCES Oferte(oid)

SELECT * FROM INFORMATION_SCHEMA.TABLES

--ALTER TABLE users ADD grade_id SMALLINT UNSIGNED NOT NULL DEFAULT 0;
--ALTER TABLE users ADD CONSTRAINT fk_grade_id FOREIGN KEY (grade_id) REFERENCES grades(id);

select * from Laptopuri

DROP TABLE Procesoare
DROP TABLE PlaciVideo
DROP TABLE Memorii
DROP TABLE SSD
DROP TABLE HDD
DROP TABLE PlaciSunet
DROP TABLE sistemeOperare
DROP TABLE Aplicatii
DROP TABLE laptopuri

select * from SistemeOperare

