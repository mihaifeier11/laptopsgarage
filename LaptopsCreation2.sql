CREATE TABLE Procesoare(pid INT IDENTITY,
	nume VARCHAR(50),
	nucleu VARCHAR(20),
	frecventaGHz FLOAT,
	PRIMARY KEY(pid))

INSERT INTO Procesoare(nume, nucleu, frecventaGHz) VALUES 
	('Intel Core i5-7200U', 'Kaby Lake', 2.5),
	('Intel Core i5-8250U', 'Kaby Lake R', 1.6),
	('Intel Core i5-5350U', 'Broadwell', 1.8),
	('Intel Core i5-7100U', 'Kaby Lake', 2.4),
	('Intel Core i7-8750U', 'Coffee Lake', 2.2),
	('Intel Core i5-8250U', 'Kaby Lake R', 1.6),
	('Intel Core i3-6006U', 'Skylake', 2.5),
	('Intel Celeron N3350', 'Apollo Lake', 1.1),
	('Intel Core i7-7700HQ', 'Kaby Lake', 2.8)


SELECT * FROM Procesoare

CREATE TABLE PlaciVideo(pvid INT IDENTITY,
	producator VARCHAR(20),
	model VARCHAR(20),
	tip VARCHAR(20),
	tipMemorie VARCHAR(20),
	dimensiuneMemorieGB INT,
	PRIMARY KEY(pvid))

INSERT INTO PlaciVideo(producator, model, tip, tipMemorie, dimensiuneMemorieGB) VALUES
	('Intel', 'HD 620', 'integrata', '-', null),
	('Intel', 'HD 6000', 'integrata', '-', null),
	('nVidia', 'GTX 1050 Ti', 'dedicata', 'GDDR5', 4),
	('Intel', 'HD 500', 'integrata', '-', null),
	('nVidia', 'GTX 1050', 'dedicata', 'GDDR5', 4)

SELECT * FROM PlaciVideo

CREATE TABLE Memorii(mid INT IDENTITY,
	tip VARCHAR(20),
	dimensiuneGB VARCHAR(20),
	frecventaMHz VARCHAR(20),
	PRIMARY KEY(mid))

INSERT INTO Memorii(tip, dimensiuneGB, frecventaMHz) VALUES
	('DDR4', 8, 2133),
	('DDR4', 8, 2400),
	('DDR3', 8, 1600),
	('DDR4', 4, 2133),
	('DDR4', 8, 2666),
	('DDR4', 4, 2400)

SELECT * FROM Memorii

CREATE TABLE SSD(ssdid INT IDENTITY,
	interfata VARCHAR(20),
	capacitate INT,
	PRIMARY KEY(ssdid))

INSERT INTO SSD(interfata, capacitate) VALUES
	('M.2', 256),
	('PCIe', 128),
	('M.2', 128)

SELECT * FROM SSD

CREATE TABLE HDD(hid INT IDENTITY,
	vitezaRPM INT,
	interfata VARCHAR(20),
	capacitateGB INT,
	tip VARCHAR(20),
	PRIMARY KEY(hid))


INSERT INTO HDD(vitezaRPM, interfata, capacitateGB, tip) VALUES
	(5400, 'SATA', 1024, '2.5 inch')

SELECT * FROM HDD

CREATE TABLE PlaciSunet(psid INT IDENTITY,
	sunet VARCHAR(20),
	numarCanale FLOAT,
	sistem VARCHAR(20),
	PRIMARY KEY(psid))

INSERT INTO PlaciSunet(sunet, numarCanale, sistem) VALUES
('HD Audio', 2.1, 'Stereo'),
('HD Audio', 2.0, 'Stereo')

SELECT * FROM PlaciSunet

CREATE TABLE SistemeOperare(soid INT IDENTITY,
	nume VARCHAR(20),
	versiune FLOAT,
	tip VARCHAR(20),
	spatiuNecesarGB FLOAT,
	PRIMARY KEY(soid))


INSERT INTO SistemeOperare(nume, versiune, tip, spatiuNecesarGB) VALUES
	('Home', 10, 'Windows', 30),
	('Student', 10, 'Windows', 20),
	('Pro', 10, 'Windows', 40),
	('Sierra', 10.12, 'macOS', 50),
	('Ubuntu', 18.04, 'Linux', 10),
	('Endless OS', 3, 'Linux', 5),
	('Free DOS', 5, 'DOS', 3)

SELECT * FROM SistemeOperare

CREATE TABLE Laptopuri(lid INT IDENTITY,
	nume VARCHAR(50),
	pret float,
	pid INT,
	pvid INT,
	mid INT,
	ssdid INT,
	hid INT,
	psid INT,
	soid INT,
	PRIMARY KEY(lid),
	FOREIGN KEY(pid) REFERENCES Procesoare(pid),
	FOREIGN KEY(pvid) REFERENCES PlaciVideo(pvid),
	FOREIGN KEY(mid) REFERENCES Memorii(mid),
	FOREIGN KEY(ssdid) REFERENCES SSD(ssdid),
	FOREIGN KEY(hid) REFERENCES HDD(hid),
	FOREIGN KEY(psid) REFERENCES PlaciSunet(psid),
	FOREIGN KEY(soid) REFERENCES SistemeOperare(soid))


CREATE TABLE Oferte(oid INT,
		lid INT,
		pret FLOAT,
		rataLunara FLOAT,
		PRIMARY KEY(oid),
		FOREIGN KEY(lid) REFERENCES Laptopuri(lid))

ALTER TABLE Laptopuri ADD oid INT
ALTER TABLE Laptopuri ADD FOREIGN KEY(oid) REFERENCES Oferte(oid)

INSERT INTO Laptopuri(nume, pret, pid, pvid, mid, ssdid, hid, psid, soid, oid) VALUES 
('Laptop ASUS Gaming 15.6" ROG GL503VD', 3998.99, 9, 5, 2, 1, null, 2, 1, null),
('Laptop HP 15.6" 250 G6', 2798.99, 1, 1, 1, 1, null, 2, 7, null),
('Laptop Lenovo 15.6" V330 IKB', 2448.99, 2, 2, 1, 1, null, 2, null, null),
('Laptop Apple 13.3" MacBook Air 13', 4098.99, 3, 2, 3, 3, null, 2, 4, null),
('Laptop ASUS 15.6" VivoBook X541UA', 1698.99, 4, 1, 4, 1, null, 2, 6, null),
('Laptop ASUS Gaming 15.6" TUF FX504GE', 1698.99, 5, 3, 5, null, 1, 2, 7, null),
('Laptop HP 15.6" 450 G5', 3298.99, 6, 1, 2, 1, null, 2, 3, null),
('Laptop DELL 15.6" Inspiron 3567', 1498.99, 7, 4, 6, null, 1, 2, 5, null),
('Laptop HP 15.6" 250 G6', 1198.99, 8, 4, 6, 3, null, 2, 7, null),
('Laptop ASUS Gaming 15.6" ROG GL503VD', 3798.99, 9, 5, 2, 1, null, 2, null, null)

SELECT * FROM Laptopuri


CREATE TABLE Aplicatii(aid INT IDENTITY,
	nume VARCHAR(20),
	scop VARCHAR(20),
	spatiuNecesarGB INT,
	PRIMARY KEY(aid))

INSERT INTO Aplicatii(nume, scop, spatiuNecesarGB) VALUES
	('MATLAB', 'Matematica', 13),
	('Visual Studio', 'Informatica', 30),
	('CLion', 'Informatica', 5),
	('PyCharm', 'Informatica', 7)


CREATE TABLE aplicatiiCompatibile(lid int REFERENCES Laptopuri(lid),
	aid int REFERENCES Aplicatii(aid),
	PRIMARY KEY(lid, aid))

INSERT INTO aplicatiiCompatibile(lid, aid) VALUES 
	(4, 1),
	(2, 2),
	(3, 4),
	(10, 1),
	(10, 2),
	(10, 3)

CREATE TABLE Mouse(moid INT IDENTITY,
	nume VARCHAR(50),
	pret FLOAT,
	PRIMARY KEY(moid))


INSERT INTO Mouse(nume, pret) VALUES 
	('Microsoft Mobile 1850 for business Black', 59.99),
	('Logitech B100 Optical USB', 31.98),
	('SteelSeries Rival 110 Black', 185.98),
	('Redragon Pegasus', 76.49),
	('Logitech G205 Prodigy', 129.99)

CREATE TABLE MouseCompatibil(lid INT REFERENCES Laptopuri(lid),
	moid INT REFERENCES Mouse(moid),
	PRIMARY KEY (lid, moid))


INSERT INTO MouseCompatibil(lid, moid) VALUES 
	(1, 1),
	(2, 2),
	(6, 3),
	(6, 5),
	(10, 3)

INSERT INTO Oferte(oid, lid, pret, rataLunara) VALUES
(1, 1, 2500, 500),
(2, 4, 3900, 600),
(3, 10, 3500, 750)

--Cerinte Fallout 4
--OS: Windows 7/8/10 (64-bit OS required) // done
--Processor: Intel Core i7 4790 2.6 GHz/AMD FX-9590 3.7 GHz or equivalent  // done
--Graphics Card: NVIDIA GTX 780 3GB/AMD Radeon R9 290X 4GB or equivalent  // done
--Memory: 8 GB RAM // done
--Storage: 30 GB free HDD space // done

SELECT L.lid, L.nume, L.pret, L.soid, L.pid, L.pvid, L.mid, L.ssdid,
	SO.soid, SO.versiune, SO.tip, SO.spatiuNecesarGB,
	P.pid, P.nume, P.frecventaGHz,
	PV.pvid, PV.producator, PV.dimensiuneMemorieGB,
	M.mid, M.dimensiuneGB,
	SSD.ssdid, SSD.capacitate
FROM Laptopuri L INNER JOIN SistemeOperare SO ON L.soid = SO.soid
	INNER JOIN Procesoare P ON L.pid = P.pid
	INNER JOIN PlaciVideo PV ON L.pvid = PV.pvid
	INNER JOIN Memorii M ON L.mid = M.mid
	INNER JOIN SSD SSD ON L.ssdid = SSD.ssdid
WHERE SO.tip = 'Windows' AND SO.versiune >= 7
	AND (P.nume LIKE '%i7-4%'
		OR P.nume LIKE '%i7-5%'
		OR P.nume LIKE '%i7-6%'
		OR P.nume LIKE '%i7-7%'
		OR P.nume LIKE '%i7-8%'
		OR P.nume LIKE '%i5-6%'
		OR P.nume LIKE '%i5-7%'
		OR P.nume LIKE '%i5-8%')
	AND P.frecventaGHz > 2.6
	AND ((PV.producator = 'nVidia' AND PV.dimensiuneMemorieGB > 3) OR
		(PV.producator = 'AMD' AND PV.dimensiuneMemorieGB > 4))
	AND M.dimensiuneGB >= 8
	AND (SSD.capacitate - SO.spatiuNecesarGB) > 30
		
ALTER TABLE Memorii ALTER COLUMN dimensiuneGB float

--Grupare placi video dupa producator, ordonate dupa avg(memorie) si numarand cate sunt din fiecare, fiind cel putin 1 la fiecare producator
SELECT PV.producator, AVG(PV.dimensiuneMemorieGB) AS [Average memory], COUNT (PV.pvid) AS [Number of]
FROM PlaciVideo PV
GROUP BY PV.producator
HAVING COUNT(PV.pvid) > 0
ORDER BY [Average memory] DESC

-- afiseaza laptopurile cu aplicatii compatibile ce au spatiul necesar mai mare de 10 GB
SELECT AC.lid, AC.aid,
	L.lid, L.nume,
	A.aid, A.nume, A.spatiuNecesarGB
FROM AplicatiiCompatibile AC INNER JOIN Laptopuri L ON AC.lid = L.lid
	INNER JOIN Aplicatii A ON AC.aid = A.aid
WHERE A.spatiuNecesarGB > 10

-- Afiseaza scopurile aplicatiilor grupate, AVG(spatiu necesar) si numarul lor, acesta trebuind sa fie cel putin 2
SELECT A.scop, AVG(A.spatiuNecesarGB) AS [Average memory], COUNT(A.aid) AS [Number of]
FROM Aplicatii A
GROUP BY A.scop
HAVING COUNT(A.aid) >= 2

--Afiseaza nucleele procesoarelor, grupate, AVG(frecventa) si numarul lor, daca AVG(frecventa) este cel putin 2
SELECT P.nucleu, AVG(P.frecventaGHz) As [Average frequency], COUNT(P.pid) AS [Number of]
FROM Procesoare P
GROUP BY P.nucleu
HAVING AVG(P.frecventaGHz) >= 2

--Afiseaza laptopurile si mouse-urile lor compatibile daca pretul mouse-ului este sub 100 lei
SELECT MC.lid, MC.moid,
	L.lid, L.nume,
	M.moid, M.nume, M.pret
FROM MouseCompatibil MC INNER JOIN Laptopuri L ON L.lid = MC.lid
	INNER JOIN Mouse M ON MC.moid = M.moid
WHERE M.pret < 100

--Afiseaza toate frecventele distincte cu dimensiunea mai mare sau egala cu 4 GB
SELECT DISTINCT M.frecventaMHz, M.dimensiuneGB
FROM Memorii M
WHERE M.dimensiuneGB >= 4
ORDER BY M.dimensiuneGB

--Arata toate sistemele de operare si tipurile lor, indiferent de versiune, o singura data
SELECT DISTINCT SO.tip, SO.nume
FROM SistemeOperare SO

--Arata toate laptopurile ce au ca procesor un Intel Core i7 si ofertele lor, daca exista
SELECT L.lid, L.pid, L.nume, L.oid,
	P.pid, P.nume,
	O.lid, O.pret, O.rataLunara, O.oid
FROM Laptopuri L INNER JOIN Procesoare P ON L.pid = P.pid
	LEFT OUTER JOIN Oferte O ON L.lid = O.lid AND O.oid = L.oid
WHERE P.nume LIKE '%i7%'

--Arata toate laptopurile ce au placa video de la nVidia si ofertele lor, daca exista
SELECT L.pvid, L.nume, L.pvid, L.lid, L.oid,
	PV.pvid, PV.producator,
	O.oid, O.lid, O.pret, O.rataLunara
FROM Laptopuri L INNER JOIN PlaciVideo PV ON L.pvid = PV.pvid
	LEFT OUTER JOIN Oferte O ON L.lid = O.lid AND O.oid = L.oid
WHERE PV.producator = 'nVidia'

--Afiseaza toate laptopurile ce au placa de sunet cu 2 canale
SELECT L.psid, L.nume, L.lid, L.oid,
	PS.psid, PS.numarCanale,
	O.oid, O.lid, O.pret, O.rataLunara
FROM Laptopuri L INNER JOIN PlaciSunet PS ON L.psid = PS.psid
	LEFT OUTER JOIN Oferte O ON L.lid = O.lid AND O.oid = L.oid
WHERE PS.numarCanale = 2.0

-- Afiseaza toate laptopurile ce au ssd cu interfata M.2 si capacitatea de minim 256
SELECT L.ssdid, L.nume,L.lid, L.oid,
	SSD.ssdid, SSD.capacitate, SSD.interfata,
	O.oid, O.lid, O.pret, O.rataLunara
FROM Laptopuri L INNER JOIN SSD SSD ON L.ssdid = SSD.ssdid
	LEFT OUTER JOIN Oferte O ON L.lid = O.lid AND O.oid = L.oid
WHERE SSD.interfata = 'M.2' AND
	SSD.capacitate >= 256


-- TOTAL 10/10
-- WHERE 5/5 // DONE
-- GROUP BY 3/3 // DONE
-- DISTINCT 2/2 // DONE
-- HAVING 2/2 // DONE
-- 2 TABELE 7/7 // DONE
-- relatii m-n 2/2 // DONE